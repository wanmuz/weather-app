import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { AboutPage } from '../pages/about/about';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { WeatherDetailPage } from '../pages/weather-detail/weather-detail'

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AppConstantsProvider } from '../providers/app-constants/app-constants';
import { WeatherApiProvider } from '../providers/weather-api/weather-api';
import { Geolocation } from '@ionic-native/geolocation';
import { HttpModule } from '@angular/http';
import { ChartModule } from 'angular2-highcharts';


@NgModule({
  declarations: [
    MyApp,
    AboutPage,
WeatherDetailPage,
    HomePage,
    TabsPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule,
    ChartModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    HomePage,
    TabsPage,
    WeatherDetailPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AppConstantsProvider,
    WeatherApiProvider,
    Geolocation,
    
  ]
})
export class AppModule {}
