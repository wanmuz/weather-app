import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {WeatherApiProvider} from '../../providers/weather-api/weather-api'
import { Geolocation } from '@ionic-native/geolocation';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

weatherInfo :any ;
weatherProv : any;
currentLocation : string;

  constructor(public navCtrl: NavController, weatherProv: WeatherApiProvider, public geolocation: Geolocation) {
	this.weatherProv = weatherProv;
 }
  weatherTapped(currentLoction){
this.geolocation.getCurrentPosition().then((position) => {
  	this.weatherProv.getCurrentWeather(position.coords.latitude, position.coords.longitude).subscribe(
        data => {

          this.weatherInfo = data
          console.log(this.weatherInfo)
          this.currentLocation = data.name
        },
        err => console.log("error is "+err), // error
        () => console.log('read users Completed '+ this.weatherInfo) // complete
    );
  	 }, (err) => {
      console.log(err);
    });

}
}
