import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { WeatherDetailPage } from '../weather-detail/weather-detail'
import {WeatherApiProvider} from '../../providers/weather-api/weather-api'
@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {

weatherList : any;
  constructor(public navCtrl: NavController, public geolocation : Geolocation, public weatherProv: WeatherApiProvider) {

  }
  ionViewDidLoad() {
    this.geolocation.getCurrentPosition().then((position) => {
  	this.weatherProv.getForecastWeather(position.coords.latitude, position.coords.longitude).subscribe(
        data => {

          this.weatherList = data.list
          console.log(this.weatherList)
        //  this.currentLocation = data.name
        },
        err => console.log("error is "+err), // error
        () => console.log('read users Completed '+ this.weatherList) // complete
    );
  	 }, (err) => {
      console.log(err);
    });
  }

  detailTapped(weather){
  	this.navCtrl.push(WeatherDetailPage, {weather:weather})

  }
}
