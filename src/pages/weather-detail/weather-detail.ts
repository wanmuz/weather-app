import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the WeatherDetailPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-weather-detail',
  templateUrl: 'weather-detail.html',
})
export class WeatherDetailPage {
	weatherInfo : any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  this.weatherInfo = navParams.get('weather');

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad WeatherDetailPage');
  }

}
